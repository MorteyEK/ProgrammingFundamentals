#include <iostream>


int main()
{
    int num1;

    std::cout << "Enter a number: "  << std::endl;

    std::cin >> num1;

    if (num1 % 2 == 0) 
    {
        for(int i = 1; i <= 20; ++i)
        {
            std::cout << i << " * " << num1 << " = " << i * num1 << std::endl;
        }
    }
    else 
    {
        int countOfFactors = 0;
        int possibleFactor = 1;

        while (countOfFactors <= 30 )
        {
            if (possibleFactor % num1 != 0)
            {
                std::cout << possibleFactor << std::endl;
                ++countOfFactors;
            }

            ++possibleFactor;
        }


    }


}